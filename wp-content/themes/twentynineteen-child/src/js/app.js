import Vue from 'vue'
import axios from 'axios'
import _ from 'lodash'
import PropertyListing from './components/PropertyList'

window.Vue = Vue;
window.axios = axios;
window._ = _;

Vue.component('property-list', PropertyListing);

new Vue({
    el: '#main'
});