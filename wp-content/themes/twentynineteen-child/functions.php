<?php

add_action('wp_enqueue_scripts', 'twentynineteen_child_styles');

/**
 * Load twenty nineteen styles.
 */
function twentynineteen_child_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_script('script', get_stylesheet_directory_uri() . '/dist/app.js', [], 1.0, true);
}
