<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php if ( ! twentynineteen_can_show_post_thumbnail() ) : ?>
        <header class="entry-header">
            <?php get_template_part( 'template-parts/header/entry', 'header' ); ?>
        </header>
    <?php endif; ?>

    <div class="entry-content">

        <iframe width="400"
                height="260"
                frameborder="0"
                scrolling="no"
                marginheight="0"
                marginwidth="0"
                src="https://maps.google.com/maps?q=<?php echo get_field('latitude'); ?>,<?php echo get_field('longitude'); ?>&hl=es;z=14&amp;output=embed"></iframe>

        <h3>Specifications</h3>

        <ul>
            <?php if (get_field('sale_rent') === 'rent') : ?>
                <li><strong>Price:</strong> £<?php echo number_format(get_field('price'), 2); ?> per week</li>
            <?php else : ?>
                <li><strong>Price:</strong> £<?php echo number_format(get_field('price'), 2); ?></li>
            <?php endif; ?>
            <li><strong>Sale Type:</strong> For <?php echo ucfirst(get_field('sale_rent')) ?></li>
            <li><strong>County:</strong> <?php echo ucfirst(get_field('county')) ?></li>
            <li><strong>Country:</strong> <?php echo ucfirst(get_field('country')) ?></li>
            <li><strong>Town:</strong> <?php echo ucfirst(get_field('town')) ?></li>
            <li><strong>Postcode:</strong> <?php echo ucfirst(get_field('postcode')) ?></li>
            <li><?php echo get_field('number_of_bedrooms') ?> Bedroom(s)</li>
            <li><?php echo get_field('number_of_bathrooms') ?> Bathroom(s)</li>
            <?php $propertyTypes = \MTCZooplaIntegration\PropertyPostType\Migrations\PropertyTypeTable::getPropertyTypes(); ?>
            <?php $propertyType = (int)get_field('property_type'); ?>
            <?php if (isset($propertyTypes[$propertyType])) : ?>
                <li><strong>Property Type:</strong> <?php echo $propertyTypes[$propertyType] ?></li>
            <?php endif; ?>
        </ul>

        <h3>Description</h3>

        <?php
        the_content(
            sprintf(
                wp_kses(
                /* translators: %s: Name of current post. Only visible to screen readers */
                    __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentynineteen' ),
                    array(
                        'span' => array(
                            'class' => array(),
                        ),
                    )
                ),
                get_the_title()
            )
        );

        wp_link_pages(
            array(
                'before' => '<div class="page-links">' . __( 'Pages:', 'twentynineteen' ),
                'after'  => '</div>',
            )
        );
        ?>

    </div><!-- .entry-content -->

    <footer class="entry-footer">
        <?php twentynineteen_entry_footer(); ?>
    </footer><!-- .entry-footer -->

    <?php if ( ! is_singular( 'attachment' ) ) : ?>
        <?php get_template_part( 'template-parts/post/author', 'bio' ); ?>
    <?php endif; ?>

</article><!-- #post-${ID} -->
