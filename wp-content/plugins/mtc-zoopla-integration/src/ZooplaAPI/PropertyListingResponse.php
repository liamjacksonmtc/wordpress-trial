<?php namespace MTCZooplaIntegration\ZooplaAPI;

if (!defined('ABSPATH')) {
    exit;
}


/**
 * Class PropertyListingResponse
 * @package MTCZooplaIntegration\ZooplaAPI
 */
class PropertyListingResponse
{

    /**
     * @var mixed
     */
    protected $country;

    /**
     * @var mixed
     */
    protected $totalResults;

    /**
     * @var mixed
     */
    protected $longitude;

    /**
     * @var mixed
     */
    protected $areaName;

    /**
     * @var mixed
     */
    protected $street;

    /**
     * @var mixed
     */
    protected $latitude;

    /**
     * @var mixed
     */
    protected $town;

    /**
     * @var mixed
     */
    protected $county;

    /**
     * @var mixed
     */
    protected $boundingBox;

    /**
     * @var mixed
     */
    protected $postcode;

    /**
     * @var array|PropertyListing[]
     */
    protected $listings;

    /**
     * PropertyListingResponse constructor.
     * @param array $response
     */
    public function __construct(array $response)
    {

        $this->country = $response['country'];
        $this->totalResults = $response['result_count'];
        $this->longitude = $response['longitude'];
        $this->areaName = $response['area_name'];
        $this->street = $response['street'];
        $this->latitude = $response['latitude'];
        $this->town = $response['town'];
        $this->county = $response['county'];
        $this->boundingBox = $response['bounding_box'];
        $this->postcode = $response['postcode'];

        $this->listings = array_map(function ($listing) {
            return new PropertyListing($listing);
        }, $response['listing']);

    }

    /**
     * @return int
     */
    public function getTotalResults()
    {
        return (int)$this->totalResults;
    }

    /**
     * @return int
     */
    public function getListingsCount()
    {
        return count($this->listings);
    }

    /**
     * @return array|PropertyListing[]
     */
    public function getListings()
    {
        return $this->listings;
    }

}