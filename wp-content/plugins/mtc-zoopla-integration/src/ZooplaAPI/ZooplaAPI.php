<?php namespace MTCZooplaIntegration\ZooplaAPI;

if (!defined('ABSPATH')) {
    exit;
}


use MTCZooplaIntegration\ZooplaAPI\Exceptions\CurlRequestException;

/**
 * Class ZooplaAPI
 * @package LJJackson\Zoopla
 */
class ZooplaAPI
{
    /**
     * @var string
     */
    protected $token;

    /**
     * ZooplaAPI constructor.
     *
     * @param string $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Returns a list of properties from the Zoopla API
     *
     * @param array $parameters
     *
     * @return PropertyListingResponse
     * @throws CurlRequestException
     */
    public function propertyListings(array $parameters)
    {

        $request = curl_init($this->buildPropertyListingsUrl($parameters));

        curl_setopt_array($request, [
            CURLOPT_RETURNTRANSFER => true
        ]);

        if (!$response = curl_exec($request)) {
            throw new CurlRequestException(curl_errno($request), curl_errno($request));
        }

        curl_close($request);

        return new PropertyListingResponse(
            json_decode($response, true)
        );

    }

    /**
     * Return the property listings URL.
     *
     * @param array $parameters
     *
     * @return string
     */
    protected function buildPropertyListingsUrl(array $parameters)
    {

        $queryString = http_build_query(array_merge($parameters, [
            'api_key' => $this->getToken()
        ]));

        return 'http://api.zoopla.co.uk/api/v1/property_listings.json?' . $queryString;

    }
}