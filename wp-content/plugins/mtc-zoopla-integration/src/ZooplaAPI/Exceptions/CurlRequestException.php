<?php namespace MTCZooplaIntegration\ZooplaAPI\Exceptions;

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class CurlRequestException extends \Exception
{
}