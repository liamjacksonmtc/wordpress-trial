<?php namespace MTCZooplaIntegration\ZooplaAPI;

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Class PropertyListing
 * @package MTCZooplaIntegration\ZooplaAPI
 */
class PropertyListing
{
    /**
     * @var int
     */
    protected $floors;

    /**
     * @var string
     */
    protected $thumbnail;

    /**
     * @var mixed
     */
    protected $latitude;

    /**
     * @var mixed
     */
    protected $longitude;

    /**
     * @var mixed
     */
    protected $category;

    /**
     * @var mixed
     */
    protected $propertyType;

    /**
     * @var mixed
     */
    protected $description;

    /**
     * @var mixed
     */
    protected $postcode;

    /**
     * @var mixed
     */
    protected $county;

    /**
     * @var float
     */
    protected $price;

    /**
     * @var mixed
     */
    protected $listingId;

    /**
     * @var mixed
     */
    protected $country;

    /**
     * @var mixed
     */
    protected $displayableAddress;

    /**
     * @var mixed
     */
    protected $listingStatus;

    /**
     * @var mixed
     */
    protected $streetName;

    /**
     * @var int
     */
    protected $bathrooms;

    /**
     * @var int
     */
    protected $bedrooms;

    /**
     * @var string
     */
    protected $town;

    /**
     * @var string
     */
    protected $detailsUrl;

    /**
     * @var string
     */
    protected $imageUrl;

    /**
     * PropertyListing constructor.
     * @param array $listing
     */
    public function __construct(array $listing)
    {
        $this->detailsUrl = $listing['details_url'];
        $this->floors = (int)$listing['num_floors'];
        $this->thumbnail = $listing['thumbnail_url'];
        $this->latitude = $listing['latitude'];
        $this->longitude = $listing['longitude'];
        $this->category = $listing['category'];
        $this->propertyType = $listing['property_type'];
        $this->description = $listing['description'];
        $this->postcode = $listing['outcode'];
        $this->county = $listing['county'];
        $this->price = (float)$listing['price'];
        $this->listingId = $listing['listing_id'];
        $this->country = $listing['country'];
        $this->displayableAddress = $listing['displayable_address'];
        $this->listingStatus = $listing['listing_status'];
        $this->streetName = $listing['street_name'];
        $this->bathrooms = (int)$listing['num_bathrooms'];
        $this->bedrooms = (int)$listing['num_bedrooms'];
        $this->town = $listing['post_town'];
        $this->imageUrl = $listing['image_url'];
    }

    /**
     * @return int
     */
    public function getFloors()
    {
        return $this->floors;
    }

    /**
     * @return string
     */
    public function getThumbnailUrl()
    {
        return $this->thumbnail;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return mixed
     */
    public function getPropertyType()
    {
        return $this->propertyType;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @return mixed
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getListingId()
    {
        return $this->listingId;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return mixed
     */
    public function getDisplayableAddress()
    {
        return $this->displayableAddress;
    }

    /**
     * @return mixed
     */
    public function getStreetName()
    {
        return $this->streetName;
    }

    /**
     * @return int
     */
    public function getBathrooms()
    {
        return $this->bathrooms;
    }

    /**
     * @return int
     */
    public function getBedrooms()
    {
        return $this->bedrooms;
    }

    /**
     * @return string
     */
    public function getListingStatus()
    {
        return $this->listingStatus;
    }

    /**
     * @return mixed|string
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * @return string
     */
    public function getDetailsUrl()
    {
        return $this->detailsUrl;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }
}