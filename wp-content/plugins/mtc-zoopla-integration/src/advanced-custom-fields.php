<?php

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Return the ACF path.
 * @return string
 */
function mtc_zoopla_integration_acf_settings_path()
{
    return get_stylesheet_directory() . '/vendor/advanced-custom-fields';
}


add_filter('acf/settings/path', 'mtc_zoopla_integration_acf_settings_path');

/**
 * Return the ACF directory URL.
 * @return string
 */
function mtc_zoopla_integration_acf_settings_url()
{
    return '/wp-content/plugins/mtc-zoopla-integration/vendor/advanced-custom-fields/';
}

add_filter('acf/settings/dir', 'mtc_zoopla_integration_acf_settings_url');

// 3. Hide ACF field group menu item
add_filter('acf/settings/show_admin', '__return_false');


require_once __DIR__ . '/../vendor/advanced-custom-fields/acf.php';