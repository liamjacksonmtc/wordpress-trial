<?php

if (!defined('ABSPATH')) {
    exit;
}


// Property Post Type
require __DIR__ . '/PropertyPostType/PropertyPostType.php';
require __DIR__ . '/PropertyPostType/Migrations/PropertyTypeTable.php';
require __DIR__ . '/PropertyPostType/Controllers/APIController.php';

// Zoopla API
require __DIR__ . '/ZooplaAPI/ZooplaAPI.php';
require __DIR__ . '/ZooplaAPI/PropertyListing.php';
require __DIR__ . '/ZooplaAPI/PropertyListingResponse.php';
require __DIR__ . '/ZooplaAPI/Exceptions/CurlRequestException.php';

// ACF Include
require __DIR__ . '/advanced-custom-fields.php';

// ZooplaPropertyImport
require __DIR__ . '/ZooplaPropertyImport/ZooplaPropertyImport.php';
require __DIR__ . '/ZooplaPropertyImport/Controllers/ManagementController.php';
require __DIR__ . '/ZooplaPropertyImport/ImportCommand.php';