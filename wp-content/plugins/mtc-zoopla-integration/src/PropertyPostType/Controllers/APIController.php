<?php namespace MTCZooplaIntegration\PropertyPostType\Controllers;

use MTCZooplaIntegration\PropertyPostType\Migrations\PropertyTypeTable;
use WP_Query;

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Class APIController
 * @package MTCZooplaIntegration\PropertyPostType\Controllers
 */
class APIController
{

    /**
     * @var []
     */
    protected $propertyTypes = null;

    /**
     * APIController constructor.
     */
    public function __construct()
    {
        add_action('rest_api_init', [$this, 'registerRoutes']);
    }

    /**
     * Register the routes for the Rest API of Properties
     * @return void
     */
    public function registerRoutes()
    {
        register_rest_route('mtc-zoopla-integration/v1', 'properties', [
            'methods' => 'GET',
            'callback' => [$this, 'index']
        ]);
    }

    /**
     * @param $key
     * @param null $default
     * @return string|null
     */
    protected function param($key, $default = null)
    {
        if (!isset($_REQUEST[$key]) || empty($_REQUEST[$key])) {
            return $default;
        }

        return strip_tags((string)wp_unslash($_REQUEST[$key]));
    }

    /**
     * Return a list of properties paginated.
     */
    public function index()
    {

        $parameters = [
            'post_type' => 'properties',
            'post_status' => 'publish',
            'paged' => (int)$this->param('page', 1),
            'posts_per_page' => (int)$this->param('limit', 20),
            'no_found_rows' => false,
        ];

        if ($name = $this->param('name')) {
            $parameters['s'] = $name;
        }

        if (!isset($parameters['meta_query'])) {
            $parameters['meta_query'] = ['relation' => 'AND'];
        }

        if ($price = $this->param('price')) {
            $parameters['meta_query'][] = [
                'key' => 'price',
                'value' => (int)$price,
                'type' => 'NUMERIC',
                'compare' => '<'
            ];
        }

        if ($saleRent = $this->param('sale_rent')) {
            $parameters['meta_query'][] = [
                'key' => 'sale_rent',
                'value' => $saleRent
            ];
        }

        $query = new WP_Query($parameters);

        wp_send_json([
            'data' => $this->mapProperties($query->get_posts()),
            'meta' => [
                'total' => (int)$query->found_posts
            ]
        ]);


    }

    /**
     * @param array|\WP_Post[] $properties
     * @return array
     */
    protected function mapProperties(array $properties)
    {

        $postMeta = $this->getPostMeta(array_column($properties, 'ID'));

        return array_map(function (\WP_Post $property) use ($postMeta) {

            $property = $property->to_array();

            $propertyMeta = array_values(array_filter($postMeta, function ($meta) use ($property) {
                return $meta['post_id'] == $property['ID'];
            }));


            foreach ($propertyMeta as $meta) {
                $property['meta'][$meta['meta_key']] = $meta['meta_value'];
            }

            foreach (['post-thumbnail', 'full', 'medium', 'large'] as $size) {

                if (!isset($property['meta']['featured_images'])) {
                    $property['meta']['featured_images'] = [];
                }

                $property['meta']['featured_images'][$size] = get_the_post_thumbnail($property['ID'], $size);

            }

            if (isset($property['meta']['property_type'])) {
                $property['meta']['property_type'] = PropertyTypeTable::getPropertyTypes()[$property['meta']['property_type']];
            }

            return $property;

        }, $properties);

    }


    /**
     * @param array $postIds
     * @return array|null
     */
    protected function getPostMeta(array $postIds)
    {

        global $wpdb;

        $placeholders = array_fill(0, count($postIds), '%d');

        $format = implode(',', $placeholders);

        $query = "
            SELECT `post_id`, `meta_key`, `meta_value`
            FROM `{$wpdb->postmeta}`
            WHERE `post_id` IN ({$format})
        ";

        return $wpdb->get_results($wpdb->prepare($query, $postIds), ARRAY_A);


    }


}