<?php namespace MTCZooplaIntegration\PropertyPostType;

use MTCZooplaIntegration\PropertyPostType\Migrations\PropertyTypeTable;

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Class PropertyPostType
 * @package MTCZooplaIntegration
 */
class PropertyPostType
{

    /**
     * PropertyPostType constructor.
     */
    public function __construct()
    {
        add_action('acf/init', [$this, 'registerFieldGroup']);
        add_action('init', [$this, 'registerPropertyPostType']);
        add_filter('acf/load_field/key=field_property_type', [$this, 'propertyTypeDropdown']);
        add_filter('acf/validate_value/key=field_property_price', [$this, 'validateNumeric'], 10, 4);
        add_filter('acf/validate_value/key=field_property_latitude', [$this, 'validateNumeric'], 10, 4);
        add_filter('acf/validate_value/key=field_property_longitude', [$this, 'validateNumeric'], 10, 4);
    }

    /**
     * @return void
     */
    public static function activation()
    {
        PropertyTypeTable::up();
    }

    /**
     * When deactivated ensure that the property type table has been dropped.
     * @return void
     */
    public static function deactivation()
    {
        PropertyTypeTable::down();
    }

    /**
     * List of all the labels for the Property post type.
     * @return array
     */
    protected function getPropertyPostTypeLabels()
    {
        return [
            'name' => _x('Properties', 'post type general name'),
            'singular_name' => _x('Property', 'post type singular name'),
            'add_new' => _x('Add New', 'property'),
            'add_new_item' => __('Add New Property'),
            'edit_item' => __('Edit Property'),
            'new_item' => __('New Property'),
            'all_items' => __('All Properties'),
            'view_item' => __('View Properties'),
            'search_items' => __('Search Properties'),
            'not_found' => __('No property found'),
            'not_found_in_trash' => __('No Property found in the Trash'),
            'parent_item_colon' => '',
            'menu_name' => 'Properties'
        ];
    }

    /**
     * Register the Property custom post type in Wordpress.
     */
    public function registerPropertyPostType()
    {

        register_post_type('properties', [
            'labels' => $this->getPropertyPostTypeLabels(),
            'description' => 'A list of Zoopla properties.',
            'public' => true,
            'menu_position' => 5,
            'supports' => ['title', 'editor', 'thumbnail', 'comments'],
            'has_archive' => true
        ]);

    }

    /**
     * @link https://www.advancedcustomfields.com/resources/register-fields-via-php/
     * @link http://www.advancedcustomfields.com/resources/dynamically-populate-a-select-fields-choices/
     * @return void
     */
    public function registerFieldGroup()
    {

        acf_add_local_field_group([
            'key' => 'properties_field_group',
            'title' => 'Properties',
            'fields' => $this->getFields(),
            'location' => [
                [
                    ['param' => 'post_type', 'operator' => '==', 'value' => 'properties']
                ]
            ]
        ]);

    }

    /**
     * @return array
     */
    protected function getFields()
    {

        $fields = [];

        // County
        $fields[] = [
            'key' => 'field_property_county',
            'label' => 'County',
            'name' => 'county',
            'type' => 'text'
        ];

        // Country
        $fields[] = [
            'key' => 'field_property_country',
            'label' => 'Country',
            'name' => 'country',
            'type' => 'text'
        ];

        // Town
        $fields[] = [
            'key' => 'field_property_town',
            'label' => 'Town',
            'name' => 'town',
            'type' => 'text'
        ];

        // Postcode
        $fields[] = [
            'key' => 'field_property_postcode',
            'label' => 'Postcode',
            'name' => 'postcode',
            'type' => 'text'
        ];

        // Description - Can be used for base description.

//        Displayable Address - Used as the title.
//        $fields[] = [
//            'key' => 'field_property_displayable_address',
//            'label' => 'Displayable Address',
//            'name' => 'displayable_address',
//            'type' => 'text'
//        ];

        // Image (File Upload) - Featured Image.
        $fields[] = [
            'key' => 'field_property_price',
            'label' => 'Price',
            'name' => 'price',
            'type' => 'text',
            'prepend' => '£'
        ];

        // Number of Bedrooms
        $fields[] = [
            'key' => 'field_property_number_of_bedrooms',
            'label' => 'Number of Bedrooms',
            'name' => 'number_of_bedrooms',
            'type' => 'select',
            'allow_null' => false,
            'multiple' => false,
            'choices' => range(0, 10)
        ];

        // Number of Bathrooms
        $fields[] = [
            'key' => 'field_property_number_of_bathrooms',
            'label' => 'Number of Bathrooms',
            'name' => 'number_of_bathrooms',
            'type' => 'select',
            'allow_null' => false,
            'multiple' => false,
            'choices' => range(0, 10)
        ];

        // Property Type (Dynamic Dropdown)
        $fields[] = [
            'key' => 'field_property_type',
            'label' => 'Property Type',
            'name' => 'property_type',
            'type' => 'select',
            'allow_null' => false,
            'multiple' => false,
            'choices' => []
        ];

        // For Sale / For Rent
        $fields[] = [
            'key' => 'field_property_sale_rent',
            'label' => 'For Sale or Rent?',
            'name' => 'sale_rent',
            'type' => 'select',
            'allow_null' => false,
            'multiple' => false,
            'choices' => ['sale' => 'For Sale', 'rent' => 'For Rent']
        ];

        // Longitude
        $fields[] = [
            'key' => 'field_property_latitude',
            'label' => 'Latitude',
            'name' => 'latitude',
            'type' => 'text'
        ];

        // Longitude
        $fields[] = [
            'key' => 'field_property_longitude',
            'label' => 'Longitude',
            'name' => 'longitude',
            'type' => 'text'
        ];

        return $fields;

    }


    /**
     * Ensure that the property type dropdown is populated from the database.
     *
     * @param array $field
     * @return array
     */
    public function propertyTypeDropdown(array $field)
    {

        global $wpdb;

        $table = PropertyTypeTable::table();

        $results = $wpdb->get_results("SELECT * FROM `{$table}` ORDER BY `name` ASC ", ARRAY_A);

        $field['choices'] = [];

        foreach ($results as $result) {
            $field['choices'][$result['id']] = $result['name'];
        }

        return $field;

    }

    /**
     * Return whether the price is valid (numeric).
     *
     * @param $valid
     * @param $value
     * @param $field
     * @param $input
     * @return string
     */
    public function validateNumeric($valid, $value, $field, $input)
    {

        if (!$valid || empty($value)) {
            return $valid;
        }

        if (!is_numeric($value)) {
            return "The property price must be a number, e.g. 100.00.";
        }

        return $valid;

    }


}
