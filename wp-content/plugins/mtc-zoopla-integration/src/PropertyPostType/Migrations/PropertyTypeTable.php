<?php namespace MTCZooplaIntegration\PropertyPostType\Migrations;

// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Class PropertyTypeTable
 * @package MTCZooplaIntegration\PropertyPostType\Migrations
 */
class PropertyTypeTable
{

    /**
     * Return the table name for the property types.
     * @return string
     */
    public static function table()
    {
        global $wpdb;
        return "{$wpdb->prefix}mtc_zoopla_property_types";
    }

    /**
     * @return void
     */
    public static function up()
    {

        global $wpdb;


        $table = self::table();

        $statement = "
            CREATE TABLE `{$table}` (
              `id` INT NOT NULL AUTO_INCREMENT ,
              `name` VARCHAR(255) NOT NULL ,
              PRIMARY KEY (`id`)
            ) {$wpdb->get_charset_collate()};
        ";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        dbDelta($statement);


    }

    /**
     * @return void
     */
    public static function down()
    {

        global $wpdb;

        $table = self::table();

        $wpdb->query("DROP TABLE IF EXISTS {$table}");

        $thumbnails = $wpdb->get_results("SELECT * FROM `{$wpdb->postmeta}` WHERE `meta_key` = '_thumbnail_id' AND `post_id` IN (
          SELECT `ID`
          FROM `{$wpdb->posts}`
          WHERE `post_type` = 'properties'
        )", ARRAY_A);

        foreach ($thumbnails as $thumbnail) {
            wp_delete_attachment($thumbnail['meta_value']);
        }

        $wpdb->query("DELETE FROM `{$wpdb->postmeta}` WHERE `post_meta` IN (
          SELECT `ID`
          FROM `{$wpdb->posts}`
          WHERE `post_type` = 'properties'
        )");

        $wpdb->query("DELETE FROM `{$wpdb->posts}` WHERE `post_type` = 'properties'");


    }

    protected static $propertyTypes = null;

    public static function getPropertyTypes()
    {

        if (is_null(self::$propertyTypes)) {
            global $wpdb;
            $table = PropertyTypeTable::table();

            $results = $wpdb->get_results("
              SELECT `id`, `name`
              FROM `{$table}`
            ", ARRAY_A);

            self::$propertyTypes = [];

            foreach ($results as $result) {
                self::$propertyTypes[$result['id']] = $result['name'];
            }

        }

        return self::$propertyTypes;
    }

}