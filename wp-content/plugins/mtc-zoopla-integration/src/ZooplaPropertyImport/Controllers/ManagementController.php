<?php namespace MTCZooplaIntegration\ZooplaPropertyImport\Pages;

if (!defined('ABSPATH')) {
    exit;
}

use MTCZooplaIntegration\ZooplaPropertyImport\ImportCommand;

/**
 * Class ManagementPage
 * @package MTCZooplaIntegration\ZooplaPropertyImport\Pages
 */
class ManagementController
{

    /**
     * ManagementPage constructor.
     */
    public function __construct()
    {

        if (!is_admin()) {
            return;
        }

        add_action('admin_menu', [$this, 'menu']);
        add_action('admin_init', [$this, 'settings']);
        add_action('admin_post_mtc_zoopla_integration_run_import', [$this, 'import']);
        add_action('init', [$this, 'sessions']);

    }

    /**
     * Start the session if it is not already active.
     * @return void
     */
    protected function sessions()
    {
        if (!session_id()) {
            session_start();
        }
    }

    /**
     * Add Zoopla API to the Menu.
     * @return void
     */
    public function menu()
    {
        add_menu_page(
            'Zoopla API',
            'Zoopla API',
            'administrator',
            'mtc-zoopla-api',
            [$this, 'render']
        );
    }

    /**
     * Register then Zoopla API Key setting.
     * @return void
     */
    public function settings()
    {
        register_setting('mtc-zoopla-integration', 'zoopla_api_key');
        register_setting('mtc-zoopla-integration', 'zoopla_api_area');
    }

    /**
     * Include the management page, this has been exported to a separate file for maintainability.
     * @return void
     * @throws \Exception
     */
    public function render()
    {

        include __DIR__ . '/../templates/management-page.php';

    }

    /**
     * @throws \Exception
     * @return void
     */
    public function import()
    {

        (new ImportCommand())->import();

        wp_redirect(
            admin_url('edit.php?post_type=properties')
        );

    }

}