<?php namespace MTCZooplaIntegration\ZooplaPropertyImport;

if (!defined('ABSPATH')) {
    exit;
}

use MTCZooplaIntegration\PropertyPostType\Migrations\PropertyTypeTable;
use MTCZooplaIntegration\ZooplaAPI\PropertyListing;
use MTCZooplaIntegration\ZooplaAPI\ZooplaAPI;

/**
 * Class ImportCommand
 * @package MTCZooplaIntegration\ZooplaPropertyImport
 */
class ImportCommand
{

    /**
     * @var int
     */
    protected $perPage = 100;

    /**
     * @var bool
     */
    protected $debug = false;

    /**
     * @var int
     */
    protected $currentPage = 1;

    /**
     * @var null|ZooplaAPI
     */
    protected $api = null;

    /**
     * @return string
     * @throws \Exception
     */
    protected function getZooplaApiKey()
    {

        if (!$token = get_option('zoopla_api_key')) {
            throw new \Exception('Import has failed, [zoopla_api_key] has not been set.');
        }

        return $token;

    }

    /**
     * @return ZooplaAPI|null
     * @throws \Exception
     */
    protected function api()
    {

        if (is_null($this->api)) {
            $this->api = new ZooplaAPI($this->getZooplaApiKey());
        }

        return $this->api;

    }


    /**
     * @return void
     * @throws \Exception
     */
    public function import()
    {

        $hasMorePages = true;

        while ($hasMorePages) {

            $response = $this->api()->propertyListings([
                'area' => get_option('zoopla_api_area', 'Dundee'),
                'page_size' => $this->perPage,
                'page_number' => $this->currentPage
            ]);

            foreach ($response->getListings() as $listing) {

                $this->importProperty($listing);

                if ($this->debug) {
                    break;
                }

            }

            if ($hasMorePages = $response->getListingsCount() > 0) {
                $this->currentPage++;
            }

            if ($this->debug) $hasMorePages = false;

        }

    }

    /**
     * @param $propertyType
     * @return false|int
     */
    protected function getPropertyTypeId($propertyType)
    {

        global $wpdb;

        $table = PropertyTypeTable::table();

        $prepared = $wpdb->prepare("SELECT `id`, `name` FROM `{$table}` WHERE `name` = %s", [$propertyType]);

        $result = $wpdb->get_row($prepared, ARRAY_A);

        if (!is_null($result)) {
            return $result['id'];
        }

        return $wpdb->insert($table, ['name' => $propertyType]);

    }

    /**
     * @param PropertyListing $listing
     * @return int|null
     */
    protected function getPostIdFromListing(PropertyListing $listing)
    {

        global $wpdb;

        $statement = "
            SELECT `post_id`
            FROM `{$wpdb->postmeta}`
            WHERE `meta_key` = '_zoopla_listing_id'
            AND `meta_value` = %s
            LIMIT 1;
        ";

        $preparedStatement = $wpdb->prepare($statement, [$listing->getListingId()]);

        $result = $wpdb->get_row($preparedStatement, ARRAY_A);

        return is_null($result) ? null : $result['post_id'];

    }

    /**
     * @param PropertyListing $listing
     */
    protected function importProperty(PropertyListing $listing)
    {

        $wpListing = [
            'post_title' => $listing->getDisplayableAddress(),
            'post_content' => $listing->getDescription(),
            'post_status' => 'publish',
            'post_type' => 'properties',
            'meta_input' => [
                '_zoopla_listing_id' => $listing->getListingId()
            ]
        ];

        if ($postId = $this->getPostIdFromListing($listing)) {
            $wpListing['ID'] = $postId;
        }

        $postId = wp_insert_post($wpListing);

        $acfMeta = [
            'county' => $listing->getCounty(),
            'country' => $listing->getCountry(),
            'town' => $listing->getTown(),
            'postcode' => $listing->getPostcode(),
            'displayable_address' => $listing->getDisplayableAddress(),
            'price' => $listing->getPrice(),
            'number_of_bedrooms' => $listing->getBedrooms(),
            'number_of_bathrooms' => $listing->getBathrooms(),
            'property_type' => $this->getPropertyTypeId($listing->getPropertyType()),
            'sale_rent' => $listing->getListingStatus(),
            'latitude' => $listing->getLatitude(),
            'longitude' => $listing->getLongitude()
        ];

        foreach ($acfMeta as $key => $value) {
            update_field($key, $value, $postId);
        }

        $this->generateAttachmentFromListing($postId, $listing);

    }

    /**
     * @param $postId
     * @param PropertyListing $listing
     */
    protected function generateAttachmentFromListing($postId, PropertyListing $listing)
    {

        if (empty($imageUrl = $listing->getImageUrl())) {
            return;
        }

        $imageName = end(explode('/', $imageUrl));


        $uploadDirectory = wp_upload_dir(); // Set upload folder
        $imageData = file_get_contents($imageUrl); // Get image data
        $uniqueFileName = wp_unique_filename($uploadDirectory['path'], $imageName); // Generate unique name
        $filename = basename($uniqueFileName); // Create image file name

        if (wp_mkdir_p($uploadDirectory['path'])) {
            $file = $uploadDirectory['path'] . '/' . $filename;
        } else {
            $file = $uploadDirectory['basedir'] . '/' . $filename;
        }


        file_put_contents($file, $imageData);

        $wpFiletype = wp_check_filetype($filename, null);

        $attachment = array(
            'post_mime_type' => $wpFiletype['type'],
            'post_title' => sanitize_file_name($filename),
            'post_content' => '',
            'post_status' => 'inherit'
        );

        $attachId = wp_insert_attachment($attachment, $file, $postId);

        require_once(ABSPATH . 'wp-admin/includes/image.php');

        $attachData = wp_generate_attachment_metadata($attachId, $file);

        wp_update_attachment_metadata($attachId, $attachData);

        set_post_thumbnail($postId, $attachId);

    }

}