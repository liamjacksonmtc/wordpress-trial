<?php namespace MTCZooplaIntegration\ZooplaPropertyImport;

if (!defined('ABSPATH')) {
    exit;
}

use MTCZooplaIntegration\ZooplaPropertyImport\Pages\ManagementController;

/**
 * Class ZooplaPropertyImport
 * @package MTCZooplaIntegration\ZooplaPropertyImport
 */
class ZooplaPropertyImport
{

    /**
     * ZooplaPropertyImport constructor.
     */
    public function __construct()
    {
        new ManagementController();
    }

}