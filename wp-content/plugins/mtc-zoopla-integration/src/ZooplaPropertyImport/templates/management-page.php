<?php if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<div class="wrap">
    <h1>Zoopla Import</h1>

    <form action="/wp-admin/admin-post.php" method="POST">
        <input type="hidden" name="action" value="mtc_zoopla_integration_run_import">
        <?php submit_button('Run the Import'); ?>
    </form>

    <form method="post" action="options.php">
        <?php settings_fields( 'mtc-zoopla-integration' ); ?>
        <?php do_settings_sections( 'mtc-zoopla-integration' ); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Zoopla API Key</th>
                <td>
                    <input type="text" name="zoopla_api_key" value="<?php echo esc_attr( get_option('zoopla_api_key') ); ?>" />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Zoopla Area</th>
                <td>
                    <input type="text" name="zoopla_api_area" value="<?php echo esc_attr( get_option('zoopla_api_area', 'Dundee') ); ?>" />
                </td>
            </tr>
        </table>

        <?php submit_button(); ?>

    </form>
</div>