<?php

if (!defined('ABSPATH')) {
    exit;
}

/*
 * Plugin Name: MTC Zoopla Integration
 */

use MTCZooplaIntegration\PropertyPostType\Controllers\APIController;
use MTCZooplaIntegration\PropertyPostType\PropertyPostType;
use MTCZooplaIntegration\ZooplaPropertyImport\ZooplaPropertyImport;

require __DIR__ . '/src/autoload.php';

new PropertyPostType;
new ZooplaPropertyImport;
new APIController;

register_activation_hook(__FILE__, [PropertyPostType::class, 'activation']);
register_deactivation_hook(__FILE__, [PropertyPostType::class, 'deactivation']);