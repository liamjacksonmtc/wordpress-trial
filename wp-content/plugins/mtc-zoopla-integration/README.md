# MTC Zoopla Integration

The overall goal of this plugin is to pull in the properties from Zoopla.

- [x] A user should be able to create and edit properties.

- [x] A user should be able to pull in Properties from Zoopla.
    - [x] Must be able to update the API Key.
    - [x] Must store in the custom property post type.
    - [x] If the property has been previously imported, then overwrite it.
    - [x] The user can update their import location.

[] A page should be created to filter properties via text search, postcode or area.